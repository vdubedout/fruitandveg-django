"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from recordingsystem.models import ProductText
from recordingsystem.models import SeasonHasLocation
from recordingsystem.models import ProductData
from recordingsystem.models import ProductType
from recordingsystem.models import Language
from recordingsystem.models import LocationInDecimal
from django.utils.datetime_safe import datetime

class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)

class ModelTesting(TestCase):
    def setUp(self):
        mNorthLoc = LocationInDecimal.objects.create(description='north',
                                 from_north_latitude=25.0000,
                                 to_south_latitude=0.0000)
        mSouthLoc = LocationInDecimal.objects.create(description='south',
                                 from_north_latitude=0.0000,
                                 to_south_latitude=-25.0000)
        mType = ProductType.objects.create(type='Fruit')
        mLang = Language.objects.create(value='French')
        mBananaData = ProductData.objects.create(product_code_name='Banana',
                                                 product_type=mType)
        mAppleData = ProductData.objects.create(product_code_name='Apple', 
                                                product_type=mType)
        mAppleText = ProductText.objects.create(name='Pomme', 
                                                language=mLang,
                                                product_data=mAppleData)
        SeasonHasLocation.objects.create(product_data=mAppleData,
                                         location=mNorthLoc,
                                         starting_month=datetime.now(),
                                         ending_month=datetime.now())
        SeasonHasLocation.objects.create(product_data=mAppleData,
                                         location=mSouthLoc,
                                         starting_month=datetime.now(),
                                         ending_month=datetime.now())
        
    def test_season_number_attached(self):
        """
        Test that the season number attached is correct
        """
        appleText = ProductText.objects.get(name='Pomme')
        seasonsNumber = appleText.seasons_number_attached()
        self.assertEqual(seasonsNumber, 2, 
                         'Wrong number of seasons attached, '+str(seasonsNumber)+' found instead of 2')
        
        
        
        
        
# -*- coding: utf-8 -*-

from tastypie.resources import ModelResource, Resource
from tastypie.authorization import Authorization
from tastypie import fields, bundle
from recordingsystem.models import ProductText, ProductData, SeasonHasLocation, Language
from tastypie.constants import ALL

class DataResource(ModelResource):
    class Meta:
        queryset = ProductData.objects.all()
        resource_name = 'product_data'
        
class TextResource(ModelResource):
    carbs_percent = fields.DecimalField(attribute = 'product_data__carbs_percent')
    class Meta:
        queryset = ProductText.objects.all()
        resource_name = 'product_text'
        
class FruitResource(ModelResource):
    # nomproduit, version, saison, imageurl
    
    class Meta:
        authorization = Authorization()
        queryset = ProductText.objects.all()
        excludes = ['description',
                    'how_to_choose',
                    'beauty_product',
                    'conservation_suggestion',
                    'suggestion',
                    'created',
                    'updated']
        resource_name='product/list'
        include_resource_uri = False
        
    def dehydrate(self, bundle):
        productText = ProductText.objects.get(pk=bundle.data['id'])
        productData = productText.product_data
        mData = {'fruit_name': bundle.data['name'],
                'version': bundle.data['version_number'] + productData.version_number,
                }
        if 'test' in bundle.request.GET:
                mData['testGet'] = bundle.request.GET['test']
        bundle.data = mData
        return bundle
    


class LanguageResource(ModelResource):
    # language, number of products    
    class Meta:
        queryset = Language.objects.all()
        fields = ['id', 'value']
        resource_name='language/list'
        
    def getNumberOfProductWithMinimumData(self, id):
        products = ProductText.objects.filter(language__id = id)
        nb = 0
        for prod in products:
            if prod.has_minimum_data():
                nb+=1
        return nb
    
    def dehydrate(self, bundle):
        bundle.data['number'] = self.getNumberOfProductWithMinimumData(bundle.data['id'])
        return bundle
        
        
    
    
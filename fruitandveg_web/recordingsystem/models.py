# -*- coding: utf-8 -*-
from django.db import models
from django.utils.datetime_safe import datetime
import os.path

class Language(models.Model):
	value = models.CharField(max_length=100, verbose_name='Langue')
	android_identifier = models.CharField(max_length=10, null=True, blank=True)
	def __unicode__(self):
		return self.value
	class Meta:
		verbose_name = 'Langue'
		verbose_name_plural = 'Langues'

# 90->23.45=North, 23.45->-23.45 equator, -23.45->-90=South
class LocationInDecimal(models.Model):
	description = models.CharField(max_length=30)
	from_north_latitude = models.DecimalField(max_digits=6, decimal_places=4)
	to_south_latitude = models.DecimalField(max_digits=6, decimal_places=4)
	from_longitude = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
	to_longitude = models.DecimalField(max_digits=7, decimal_places=4, blank=True, null=True)
	def __unicode__(self):
		return self.description
	class Meta:
		verbose_name = 'Zone géographique'
		verbose_name_plural = 'Zones géographiques'

#Fruit, Vegetable, Mushroom, (noix?)
class ProductType(models.Model):
	type = models.CharField('Type du Produit', max_length=60)
	def __unicode__(self):
		return self.type
	class Meta:
		verbose_name = 'Type de produit'
		verbose_name_plural = 'Type des produits'

class ProductData(models.Model):
	product_code_name = models.CharField('Nom du produit Anglais et Francais', 
										max_length=80, 
										help_text='Exemple : "Apple (Pomme)" ou "Tomato (Tomate)"')
	product_type = models.ForeignKey(ProductType, verbose_name='Type de produit')
	# productpictures/productname/date_productname.jpg
	def upload_path(self, filename):
		mDate = datetime.now()
		mFilename = mDate.strftime('%Y-%m-%d_%H-%M-%S')
		mFilename += '_' + self.product_code_name + filename[filename.index('.'):]
		uploadpath = os.path.join('productpictures', self.product_code_name, mFilename)
		return uploadpath
	product_picture = models.ImageField(upload_to=upload_path, verbose_name='Image', blank=True)
	
	carbs_percent = models.DecimalField('Glucides (%)', max_digits=5, decimal_places=2, null=True, blank=True)
	fat_percent = models.DecimalField('Lipides (%)', max_digits=5, decimal_places=2, null=True, blank=True)
	protein_percent = models.DecimalField('Proteines (%)', max_digits=5, decimal_places=2, null=True, blank=True)
	calories = models.DecimalField('calories pour 100g', 
								max_digits=10, 
								decimal_places=2, 
								null=True, 
								blank=True, 
								help_text='1000 calories = 1 Calorie (avec un grand C)')
	fridge_conservation_in_days = models.IntegerField('Réfrigérateur', null=True, blank=True)
	air_conservation_in_days = models.IntegerField('A l\'air libre', null=True, blank=True)
	version_number = models.IntegerField(default=0, null=True, blank=True)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	def __unicode__(self):
		return self.product_code_name
	def save(self, force_insert=False, force_update=False, *args, **kwargs):
		self.version_number += 1
		super(ProductData, self).save(force_insert, force_update, *args, **kwargs)
	class Meta:
		verbose_name = 'Données produit'
		verbose_name_plural = 'Données produits'


class ProductText(models.Model):
	name = models.CharField('Nom traduit', max_length=100)
	
	language = models.ManyToManyField(Language, verbose_name='Langue')
	def languages_value(self):
		return ', '.join([lang.value for lang in self.language.all()])
	languages_value.short_description = 'Langues'
	
	product_data = models.ForeignKey(ProductData, 
									verbose_name='Produit', 
									help_text='Choisir le produit concerné')
	description = models.TextField('Description', null=True, blank=True)
	how_to_choose = models.TextField('Comment le choisir', null=True, blank=True)
	beauty_product = models.TextField('Produit de beauté existant', null=True, blank=True)
	conservation_suggestion = models.TextField('Conseils de conservation', 
											help_text='Décrire le meilleur moyen de conserver le produit', 
											null=True, 
											blank=True)
	suggestion = models.TextField('Conseils divers', null=True, blank=True)
	version_number = models.IntegerField(default=0, null=True, blank=True)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	def __unicode__(self):
		return self.name
	
	def seasons_number_attached(self):
		seasons = SeasonHasLocation.objects.filter(product_data__pk=self.product_data.pk)
		return len(seasons)
	seasons_number_attached.short_description = 'Saisons remplies'	

	def has_minimum_data(self):
		if self.description is not None and	\
		self.product_data.carbs_percent is not None and \
		self.product_data.fat_percent is not None and \
		self.product_data.protein_percent is not None and \
		self.seasons_number_attached() > 0:
			return True
		else:
			return False
	has_minimum_data.boolean = True
	has_minimum_data.short_description = 'Possède les données minimum?'
	
	def save(self, force_insert=False, force_update=False, *args, **kwargs):
		self.version_number += 1
		super(ProductText, self).save(force_insert, force_update, *args, **kwargs)
	
	class Meta:
		verbose_name = 'Textes produit'
		verbose_name_plural = 'Textes produits'


class SeasonHasLocation(models.Model):
	product_data = models.ForeignKey(ProductData, verbose_name='Produit')
	location = models.ForeignKey(LocationInDecimal, verbose_name='Zone géographique')
	starting_month = models.DateField('Début pleine saison')
	ending_month = models.DateField('Fin pleine saison')
	updated = models.DateTimeField(auto_now=True)
	def __unicode__(self):
		return self.product_data.product_code_name + " -> " + self.location.description
	class Meta:
		verbose_name = 'Saison du produit'
		verbose_name_plural = 'Saisons des produits'


class RecipeLink(models.Model):
	product_text = models.ForeignKey(ProductText, verbose_name='Textes produit')
	description = models.CharField(max_length=200, verbose_name='Description du lien')
	link = models.URLField('URL du lien')
	def __unicode__(self):
		return self.description
	class Meta:
		verbose_name = 'Recette'
		verbose_name_plural = 'Recettes'


class OtherLink(models.Model):
	product_text = models.ForeignKey(ProductText, verbose_name='Textes produit')
	description = models.CharField(max_length=200, verbose_name='Description du lien')
	link = models.URLField('URL du lien')
	def __unicode__(self):
		return self.description
	class Meta:
		verbose_name = 'Lien'
		verbose_name_plural = 'Liens'

	
	


# -*- coding: utf-8 -*-
from django.contrib import admin
from recordingsystem.models import ProductText, ProductData
from recordingsystem.models import Language
from recordingsystem.models import RecipeLink, OtherLink
from recordingsystem.models import SeasonHasLocation, LocationInDecimal
from recordingsystem.models import ProductType

class RecipeLinkAdmin(admin.TabularInline):
	model = RecipeLink
	extra = 1

class OtherLinkAdmin(admin.TabularInline):
	model = OtherLink
	extra = 1

class ProductDataAdmin(admin.ModelAdmin):
	fieldsets = [
		('Base', {'fields' : [
				('product_code_name', 'product_type'),
				'product_picture',
			]} 
		),
		('Nutrition', {'fields' : [
				('protein_percent', 'carbs_percent', 'fat_percent'),
				'calories'
			]}
		),
		('Durée de conservation en jours', {'fields' : [
				('air_conservation_in_days', 'fridge_conservation_in_days')
			]}
		)
	]
	
	list_display = ('product_code_name', 'product_type', 'calories', 'updated', 'created')

class ProductTextAdmin(admin.ModelAdmin):
	fieldsets = [
		('Informations principales', {'fields': [
			'product_data',
			'language',
			'name',
			'description', 
		]}),
		('Informations secondaires', {'fields': [
			'how_to_choose',
			'conservation_suggestion',
			'suggestion',
			'beauty_product',
		]}),
	]
	inlines = [RecipeLinkAdmin, OtherLinkAdmin]
	
	list_display = ('name', 'languages_value', 'seasons_number_attached', 'has_minimum_data', 'updated', 'created')
	

class SeasonHasLocationAdmin(admin.ModelAdmin):
	list_display = ('product_data', 'location', 'starting_month', 'ending_month')

admin.site.register(ProductText, ProductTextAdmin)
admin.site.register(ProductData, ProductDataAdmin)
admin.site.register(Language)
admin.site.register(SeasonHasLocation, SeasonHasLocationAdmin)
admin.site.register(LocationInDecimal)
admin.site.register(ProductType)
